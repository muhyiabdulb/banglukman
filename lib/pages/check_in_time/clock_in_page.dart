import 'dart:async';
import 'dart:io';
import 'package:bang_lukman/pages/attendance/attendance_page.dart';
import 'package:flutter/material.dart';
import 'package:bang_lukman/shared/theme.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';

class ClockInPage extends StatefulWidget {

  @override
  _ClockInPageState createState() => _ClockInPageState();
}

class _ClockInPageState extends State<ClockInPage> {
  Set<Marker> _markers = {};

  Completer<GoogleMapController> _controller = Completer();
  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _markers.add(
        Marker(
          markerId: MarkerId('id-1'), 
          position: LatLng(-6.6391916, 106.8388135),
          infoWindow: InfoWindow(
            title: 'Robust Creative',
            snippet: 'Bogor, Tajur'
          )
        ),
      );
    });
  }

  static final CameraPosition _position = CameraPosition(
    
    target: LatLng(-6.6391916, 106.8388135),
    zoom: 25
  );

  Future<void> _getToPosition() async{
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_position));
  }

  Widget button(Function function, IconData icon){
    return FloatingActionButton(
      onPressed: function,
      materialTapTargetSize: MaterialTapTargetSize.padded,
      backgroundColor: Colors.white,
      child: Icon(
        icon, 
        size: 36.0,
        color: bgColor,
      ),
    );
  }

  File _image;
  final imagePicker = ImagePicker();
  Future getImage() async{
    final image = await imagePicker.getImage(source: ImageSource.camera);
    setState(() {
      _image = File(image.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backwardsCompatibility: false,
        centerTitle: true,
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(8),
            bottomLeft: Radius.circular(8),
          ),
        ),
        toolbarHeight: 70,
        title: Text(
          'CLOCK IN',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 16,
          ),
        ),
        backgroundColor: bgColor,
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            onMapCreated: _onMapCreated,
            markers: _markers,
            initialCameraPosition: CameraPosition(
              target: LatLng(-6.6392982,106.8388265),
              zoom: 25
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.topRight,
              child: Column(
                children: <Widget>[
                  button(_getToPosition, Icons.location_searching,)                 
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 340),
            child: Container(
              height: 380,
              width: double.infinity,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 4,
                      blurRadius: 10,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(20),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:[
                  Padding(
                    padding: const EdgeInsets.only(top: 50, right: 25, left: 25),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 192,
                          width: double.infinity,
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Color(0xffA9A9A9),
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Center(
                              child: 
                                _image == null ? Padding(
                                  padding: const EdgeInsets.only(top: 50, bottom: 50),
                                  child: Column(
                                    children: [
                                      IconButton(
                                        icon: Icon(
                                          Icons.upload_file_rounded,
                                          color: bgColor,
                                          size: 40.0,
                                        ),
                                        onPressed: getImage,
                                      ),
                                      Text('Files Support JPG, PNG')
                                    ],
                                  ),
                                ) : Column(
                                      children: [
                                        Expanded(
                                          child: InkWell(    
                                            onTap: getImage,                                
                                            child: Image.file(_image),
                                      ),
                                        ),
                                  ],
                                ),
                            ),
                            
                          ),
                        ),
                        SizedBox(
                          height: 42,
                        ),
                        SizedBox(
                          height: 50,
                          width: 150,
                          child: ElevatedButton(
                            child: Text('CLOCK IN'),
                            onPressed: () {
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(builder: (context) => AttendancePage())
                              );
                              print('ini notifikasi');
                            },
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                //to set border radius to button
                                borderRadius: BorderRadius.circular(4),
                              ),
                              primary: Color(0xff46D2A8),
                              // padding:
                              //     EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                              textStyle: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 13,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    
                  ),
                ]
              ),
            ),
          ),
        ],
      ),
      
    );
  }
}