import 'package:bang_lukman/shared/theme.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16,
              right: 16,
              top: 16,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: logoColor,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    // SizedBox(
                    //   width: 80,
                    // ),
                    Align(
                      heightFactor: 2,
                      alignment: Alignment.center,
                      child: Text(
                        'Register',
                        style: TextStyle(
                          color: fontColor,
                          fontSize: 18,
                          // fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 60,
                ),
                TextFormField(
                  cursorColor: bgColor,
                  // autofocus: true,
                  keyboardType: TextInputType.emailAddress,
                  // style textfield
                  style: new TextStyle(
                      fontWeight: FontWeight.normal, color: Colors.black),
                  decoration: InputDecoration(
                    // buat background warna texfield
                    fillColor: Colors.white, filled: true,
                    hintText: 'Email Address',
                    hintStyle: TextStyle(
                      color: Colors.black,
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 17.0,
                      horizontal: 20.0,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                SizedBox(
                  height: 17.08,
                ),
                TextFormField(
                  cursorColor: bgColor,
                  // autofocus: true,
                  obscureText: true,
                  keyboardType: TextInputType.visiblePassword,
                  // style textfield
                  style: new TextStyle(
                      fontWeight: FontWeight.normal, color: Colors.black),
                  decoration: InputDecoration(
                    // buat background warna texfield
                    fillColor: Colors.white, filled: true,
                    hintText: 'Password',
                    hintStyle: TextStyle(
                      color: Colors.black,
                    ),
                    suffixIcon: Icon(
                      Icons.remove_red_eye_outlined,
                      color: Colors.grey,
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 17.0,
                      horizontal: 20.0,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                SizedBox(
                  height: 17.08,
                ),
                TextFormField(
                  cursorColor: bgColor,
                  // autofocus: true,
                  obscureText: true,
                  keyboardType: TextInputType.visiblePassword,
                  initialValue: 'Retype Password',
                  // style textfield
                  style: new TextStyle(
                      fontWeight: FontWeight.normal, color: Colors.black),
                  decoration: InputDecoration(
                    // buat background warna texfield
                    fillColor: Colors.white, filled: true,
                    hintText: 'Retype Password',
                    hintStyle: TextStyle(
                      color: Colors.black,
                    ),
                    suffixIcon: Icon(
                      Icons.visibility_off_outlined,
                      color: Colors.grey,
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 17.0,
                      horizontal: 20.0,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                SizedBox(
                  height: 17.08,
                ),
                TextFormField(
                  cursorColor: bgColor,
                  keyboardType: TextInputType.phone,
                  // style textfield
                  style: new TextStyle(
                      fontWeight: FontWeight.normal, color: Colors.black),
                  decoration: InputDecoration(
                    // buat background warna texfield
                    fillColor: Colors.white, filled: true,
                    hintText: 'Phone Number',
                    hintStyle: TextStyle(
                      color: Colors.black,
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 17.0,
                      horizontal: 20.0,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                SizedBox(
                  height: 17.08,
                ),
                Center(
                  child: SizedBox(
                    height: 56,
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                          // garis pinggir
                          side: BorderSide(
                            color: bgColor,
                            width: 1,
                          ),
                        ),
                        // bayang
                        elevation: 7,
                        primary: buttonColor,
                        padding: EdgeInsets.only(
                          left: 24,
                          right: 24,
                        ),
                      ),
                      onPressed: () {
                        print('register button');
                      },
                      child: Text(
                        'REGISTER',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
