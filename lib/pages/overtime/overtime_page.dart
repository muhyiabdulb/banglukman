import 'package:bang_lukman/shared/theme.dart';
import 'package:flutter/material.dart';

class OverTimePage extends StatefulWidget {
  @override
  _OverTimePageState createState() => _OverTimePageState();
}

class _OverTimePageState extends State<OverTimePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backwardsCompatibility: false,
        centerTitle: true,
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(8),
            bottomLeft: Radius.circular(8),
          ),
        ),
        toolbarHeight: 70,
        title: Text(
          'OVERTIME',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 16,
          ),
        ),
        backgroundColor: bgColor,
      ),
      body: ListView(
        children: <Widget>[
          SingleChildScrollView(
            padding: const EdgeInsets.fromLTRB(21, 86, 21, 100),
            scrollDirection: Axis.vertical,
            child: Table(
              children: [
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 32,
                                decoration: BoxDecoration(
                                  color: Color(0xff49A388),
                                  border: Border.all(
                                    width: 2,
                                    color: Color(0xff4D8D22),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'TGL',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 91,
                                height: 32,
                                decoration: BoxDecoration(
                                  color: Color(0xff49A388),
                                  border: Border.all(
                                    width: 2,
                                    color: Color(0xff4D8D22),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'JAM MASUK',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 102,
                                height: 32,
                                decoration: BoxDecoration(
                                  color: Color(0xff49A388),
                                  border: Border.all(
                                    width: 2,
                                    color: Color(0xff4D8D22),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'JAM KELUAR',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 32,
                                decoration: BoxDecoration(
                                  color: Color(0xff49A388),
                                  border: Border.all(
                                    width: 2,
                                    color: Color(0xff4D8D22),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'STATUS',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4),
                                    topRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4),
                                    topRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4),
                                    topRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4),
                                    topRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(4),
                          3: FlexColumnWidth(4),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(4),
                                    bottomRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(4),
                                    bottomRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(4),
                                    bottomRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '22:00',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(4),
                                    bottomRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Terlaksana',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
