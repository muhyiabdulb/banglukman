import 'dart:async';
import 'package:bang_lukman/pages/login/login_page.dart';
import 'package:bang_lukman/shared/theme.dart';
import 'package:flutter/material.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  void initState() {
    super.initState();
    startSplashScreen();
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 5);
    return Timer(duration, () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) {
          return LoginPage();
        }),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: Center(
          child: CircleAvatar(
            radius: 80,
            backgroundColor: Colors.white,
            child: Text(
              'Logo',
              style: TextStyle(
                color: bgColor,
                fontSize: 25,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
