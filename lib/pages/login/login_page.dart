import 'package:bang_lukman/pages/home/main_home_page.dart';
import 'package:bang_lukman/pages/register/register_pade.dart';
import 'package:bang_lukman/shared/theme.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16,
              right: 16,
              top: 16,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: logoColor,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    // SizedBox(
                    //   width: 80,
                    // ),
                    Align(
                      heightFactor: 2,
                      alignment: Alignment.center,
                      child: Text(
                        'Log in',
                        style: TextStyle(
                          color: fontColor,
                          fontSize: 18,
                          // fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 100,
                ),
                TextFormField(
                  cursorColor: bgColor,
                  keyboardType: TextInputType.emailAddress,
                  // autofocus: true,
                  // style textfield
                  style: new TextStyle(
                      fontWeight: FontWeight.normal, color: Colors.black),
                  decoration: InputDecoration(
                    // buat background warna texfield
                    fillColor: Colors.white, filled: true,
                    hintText: 'Email Address',
                    hintStyle: TextStyle(
                      color: Colors.black,
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 17.0,
                      horizontal: 20.0,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                SizedBox(
                  height: 17.08,
                ),
                TextFormField(
                  cursorColor: bgColor,
                  // autofocus: true,
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: true,
                  // style textfield
                  style: new TextStyle(
                      fontWeight: FontWeight.normal, color: Colors.black),
                  decoration: InputDecoration(
                    // buat background warna texfield
                    fillColor: Colors.white, filled: true,
                    hintText: 'Password',
                    hintStyle: TextStyle(
                      color: Colors.black,
                    ),
                    suffixIcon: Icon(
                      Icons.remove_red_eye_outlined,
                      color: Colors.grey,
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 17.0,
                      horizontal: 20.0,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                SizedBox(
                  height: 1,
                ),
                InkWell(
                  onTap: () {
                    print('Forgot Password text');
                  },
                  child: Text(
                    'Forgot Password?',
                    style: TextStyle(
                      color: fontColor,
                    ),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Center(
                  child: SizedBox(
                    height: 56,
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8), // <-- Radius
                        ),
                        primary: bgColor,
                        padding: EdgeInsets.only(
                          left: 24,
                          right: 24,
                        ),
                        // bayang
                        elevation: 7,
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MainHomePage(),
                          ),
                        );
                        print('login button');
                      },
                      child: Text(
                        'LOG IN',
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Checkbox(
                    //   checkColor: Colors.white,
                    //   fillColor: MaterialStateProperty.all(bgColor),
                    //   value: isChecked,
                    //   onChanged: (bool value) {
                    //     setState(() {
                    //       isChecked = value;
                    //     });
                    //   },
                    // ),
                    Text(
                      'Don’t have an account?',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RegisterPage(),
                          ),
                        );
                      },
                      child: Text(
                        'Sign up',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          color: fontColor,
                        ),
                      ),
                    ),
                  ],
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                //   children: [
                //     Padding(
                //       padding: EdgeInsets.symmetric(horizontal: 0.0),
                //       child: Container(
                //         height: 1.0,
                //         width: 140.0,
                //         color: Colors.black12,
                //       ),
                //     ),
                //     Text(
                //       'OR',
                //       style: TextStyle(
                //         fontSize: 18,
                //         fontWeight: FontWeight.w500,
                //       ),
                //     ),
                //     Padding(
                //       padding: EdgeInsets.symmetric(horizontal: 0.0),
                //       child: Container(
                //         height: 1.0,
                //         width: 140.0,
                //         color: Colors.black12,
                //       ),
                //     ),
                //   ],
                // ),
                // SizedBox(
                //   height: 20,
                // ),
                // Center(
                //   child: Container(
                //     width: 327,
                //     height: 56,
                //     child: OutlinedButton(
                //       child: Row(
                //         mainAxisAlignment: MainAxisAlignment.start,
                //         children: [
                //           Container(
                //             child: ClipOval(
                //               child: Image.asset(
                //                 'assets/img/google.png',
                //                 height: 20,
                //                 width: 20,
                //                 fit: BoxFit.cover,
                //               ),
                //             ),
                //           ),
                //           SizedBox(
                //             width: 45,
                //           ),
                //           Text(
                //             'Sign in with Google',
                //             style: TextStyle(
                //               color: Colors.black,
                //               fontSize: 16,
                //               fontWeight: FontWeight.w500,
                //             ),
                //           ),
                //         ],
                //       ),
                //       style: OutlinedButton.styleFrom(
                //         shape: RoundedRectangleBorder(
                //           borderRadius: BorderRadius.circular(8), // <-- Radius
                //         ),
                //         side: BorderSide(
                //           width: 1,
                //           color: Colors.black26,
                //         ),
                //       ),
                //       onPressed: () {
                //         print('login google');
                //       },
                //     ),
                //   ),
                // ),
                // SizedBox(
                //   height: 16,
                // ),
                // Center(
                //   child: Container(
                //     width: 327,
                //     height: 56,
                //     child: OutlinedButton(
                //       child: Row(
                //         mainAxisAlignment: MainAxisAlignment.start,
                //         children: [
                //           Container(
                //             child: ClipOval(
                //               child: Image.asset(
                //                 'assets/img/apple.jpg',
                //                 height: 20,
                //                 width: 20,
                //                 fit: BoxFit.cover,
                //               ),
                //             ),
                //           ),
                //           SizedBox(
                //             width: 60,
                //           ),
                //           Text(
                //             'Sign in with Apple',
                //             style: TextStyle(
                //               color: Colors.black,
                //               fontSize: 16,
                //               fontWeight: FontWeight.w500,
                //             ),
                //             textAlign: TextAlign.justify,
                //           ),
                //         ],
                //       ),
                //       style: OutlinedButton.styleFrom(
                //         shape: RoundedRectangleBorder(
                //           borderRadius: BorderRadius.circular(8), // <-- Radius
                //         ),
                //         side: BorderSide(
                //           width: 1,
                //           color: Colors.black26,
                //         ),
                //       ),
                //       onPressed: () {
                //         print('login apple');
                //       },
                //     ),
                //   ),
                // ),
                // SizedBox(
                //   height: 16,
                // ),
                // Center(
                //   child: Container(
                //     width: 327,
                //     height: 56,
                //     child: OutlinedButton(
                //       child: Row(
                //         mainAxisAlignment: MainAxisAlignment.start,
                //         children: [
                //           Container(
                //             child: ClipOval(
                //               child: Image.asset(
                //                 'assets/img/fb.png',
                //                 height: 20,
                //                 width: 20,
                //                 fit: BoxFit.cover,
                //               ),
                //             ),
                //           ),
                //           SizedBox(
                //             width: 40,
                //           ),
                //           Text(
                //             'Sign in with Facebook',
                //             style: TextStyle(
                //               color: Colors.black,
                //               fontSize: 16,
                //               fontWeight: FontWeight.w500,
                //             ),
                //           ),
                //         ],
                //       ),
                //       style: OutlinedButton.styleFrom(
                //         shape: RoundedRectangleBorder(
                //           borderRadius: BorderRadius.circular(8), // <-- Radius
                //         ),
                //         side: BorderSide(
                //           width: 1,
                //           color: Colors.black26,
                //         ),
                //       ),
                //       onPressed: () {
                //         print('login fb');
                //       },
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
