import 'dart:ui';
import 'package:bang_lukman/pages/check_in_time/clock_in_page.dart';
import 'package:bang_lukman/pages/home/component/announcement_page.dart';
import 'package:bang_lukman/pages/home/component/menu_page.dart';
import 'package:bang_lukman/pages/home/component/submission_list_page.dart';
import 'package:bang_lukman/pages/notifikasi/notifikasi_page.dart';
import 'package:bang_lukman/pages/personal_data/personal_data_page.dart';
import 'package:bang_lukman/shared/theme.dart';
import 'package:flutter/material.dart';

class MainHomePage extends StatefulWidget {
  @override
  _MainHomePageState createState() => _MainHomePageState();
}

class _MainHomePageState extends State<MainHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Stack(
                children: [
                  Container(
                    height: 200,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 4,
                          blurRadius: 10,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ],
                      color: bgColor,
                      borderRadius: BorderRadius.vertical(
                        bottom: Radius.circular(8),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 37),
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Positioned(
                              child: IconButton(
                                icon: Icon(
                                  Icons.person,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => PersonalDataPage(),
                                    ),
                                  );
                                  print('ini profile');
                                },
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          right: 11,
                          child: IconButton(
                            icon: Icon(
                              Icons.notifications_sharp,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NotifikasiPage(),
                                ),
                              );
                              print('ini notifikasi');
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 60, left: 30, right: 30),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: 42,
                          backgroundColor: Colors.white,
                          child: CircleAvatar(
                            radius: 41,
                            backgroundColor: bgColor,
                            child: CircleAvatar(
                              radius: 36,
                              backgroundColor: logoColor,
                              child: Text(
                                'Logo',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RichText(
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.start,
                                text: TextSpan(
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  children: <TextSpan>[                                  
                                    TextSpan(
                                      text: 'Lukman Firdaus',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Text(
                                'Project Manager',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                                textAlign: TextAlign.start,
                              ),
                              RichText(
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.start,
                                text: TextSpan(
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                  children: <TextSpan>[                                  
                                    TextSpan(
                                      text: 'Robust Creative Tajur',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(30, 170, 30, 0),
                    child: Container(
                      height: 115,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 4,
                            blurRadius: 10,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 20,
                              right: 20,
                              top: 10,
                              bottom: 10,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 3,
                                  ),
                                  child: Text(
                                    'Clock In',
                                    style: TextStyle(
                                      // fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: 20,
                                  ),
                                  child: Text(
                                    'You have clock in yet',
                                    style: TextStyle(
                                      fontSize: 11,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 7,
                                    bottom: 10,
                                  ),
                                  child: Text(
                                    'Have a nice day...',
                                    style: TextStyle(
                                      color: logoColor,
                                      fontSize: 11,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, top: 5, bottom: 5),
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ClockInPage(),
                                  ),
                                );
                                print('ini ClocInPage');
                              },
                              child: CircleAvatar(
                                radius: 50,
                                backgroundColor: logoColor,
                                child: CircleAvatar(
                                  radius: 49,
                                  backgroundColor: Colors.white,
                                  child: CircleAvatar(
                                    radius: 44,
                                    backgroundColor: logoColor,
                                    child: Text(
                                      'Clock In',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              // Page Menu
              MenuPage(),
              // List Submission
              SubmissionListPage(),
              AnnouncementPage()
            ],
          ),
        ),
      ),
    );
  }
}
