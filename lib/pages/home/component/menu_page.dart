import 'package:bang_lukman/pages/attendance/attendance_page.dart';
import 'package:bang_lukman/pages/claim/claim_page.dart';
import 'package:bang_lukman/pages/overtime/overtime_page.dart';
import 'package:bang_lukman/pages/paid_leave/paid_leave_page.dart';
import 'package:flutter/material.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30, 30, 30, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AttendancePage(),
                    ),
                  );
                  print('ini attendence');
                },
                child: Card(
                  child: CircleAvatar(
                    backgroundColor: Color(0xffF3F3F3),
                    radius: 30,
                    child: Image.asset(
                      'assets/img/attendance.png',
                      width: 30,
                      height: 30,
                    ),
                  ),
                  elevation: 5,
                  shape: CircleBorder(),
                  clipBehavior: Clip.antiAlias,
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Text(
                'ATTENDANCE',
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ClaimPage(),
                    ),
                  );
                  print('ini approval');
                },
                child: Card(
                  child: CircleAvatar(
                    backgroundColor: Color(0xffF3F3F3),
                    radius: 30,
                    child: Image.asset(
                      'assets/img/claim.png',
                      width: 30,
                      height: 30,
                    ),
                  ),
                  elevation: 5,
                  shape: CircleBorder(),
                  clipBehavior: Clip.antiAlias,
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Text(
                'CLAIM',
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PaidLeavePage(),
                    ),
                  );
                },
                child: Card(
                  child: CircleAvatar(
                    backgroundColor: Color(0xffF3F3F3),
                    radius: 30,
                    child: Image.asset(
                      'assets/img/paid leave.png',
                      width: 50,
                      height: 50,
                    ),
                  ),
                  elevation: 5,
                  shape: CircleBorder(),
                  clipBehavior: Clip.antiAlias,
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Text(
                'PAID LEAVE',
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OverTimePage(),
                    ),
                  );
                },
                child: Card(
                  child: CircleAvatar(
                    backgroundColor: Color(0xffF3F3F3),
                    radius: 30,
                    child: Image.asset(
                      'assets/img/time.png',
                      width: 50,
                      height: 50,
                    ),
                  ),
                  elevation: 5,
                  shape: CircleBorder(),
                  clipBehavior: Clip.antiAlias,
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Text(
                'OVERTIME',
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                },
                child: Card(
                  child: CircleAvatar(
                    backgroundColor: Color(0xffF3F3F3),
                    radius: 30,
                    child: Image.asset(
                      'assets/img/wallet.png',
                      width: 30,
                      height: 30,
                    ),
                  ),
                  elevation: 7,
                  shape: CircleBorder(),
                  clipBehavior: Clip.antiAlias,
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Text(
                'E-PAYSLIP',
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
