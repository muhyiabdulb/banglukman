import 'package:flutter/material.dart';

class SubmissionListPage extends StatefulWidget {

  @override
  _SubmissionListPageState createState() => _SubmissionListPageState();
}

class _SubmissionListPageState extends State<SubmissionListPage> {
  @override
  Widget build(BuildContext context) {
    return 
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 20, 30, 0),
            child: Table(
              children: [
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(3),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                // width: 58,
                                height: 32,
                                decoration: BoxDecoration(
                                  color: Color(0xff49A388),
                                  border: Border.all(
                                    width: 2,
                                    color: Color(0xff4D8D22),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Tanggal',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 91,
                                height: 32,
                                decoration: BoxDecoration(
                                  color: Color(0xff49A388),
                                  border: Border.all(
                                    width: 2,
                                    color: Color(0xff4D8D22),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Kategori',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: 102,
                                height: 32,
                                decoration: BoxDecoration(
                                  color: Color(0xff49A388),
                                  border: Border.all(
                                    width: 2,
                                    color: Color(0xff4D8D22),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Berakhir',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(3),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                // width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4),
                                    topRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                // width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4),
                                    topRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Liburan luar Negeri',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              Container(
                                // width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4),
                                    topRight: Radius.circular(4),
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(3),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                // width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                // width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Liburan luar Negeri',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(3),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                // width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                // width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Liburan luar Negeri',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              Container(
                                // width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '7/3/21',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          2: FlexColumnWidth(3),
                        },
                        children: [
                          TableRow(
                            children: [
                              Container(
                                // width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                // width: 58,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Color(0xff46D2A8),
                                  border: Border.all(
                                    width: 0,
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    '',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
  }
}
    