import 'package:bang_lukman/pages/home/main_home_page.dart';
import 'package:bang_lukman/pages/paid_leave/result_paid_leave_page.dart';
import 'package:bang_lukman/shared/theme.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PaidLeavePage extends StatefulWidget {
  @override
  _PaidLeavePageState createState() => _PaidLeavePageState();
}

class _PaidLeavePageState extends State<PaidLeavePage> {
  // String _fileName;
  // String _path;
  // Map<String, String> _paths;
  // String _extension;
  // bool _loadingPath = false;
  // bool _multiPick = false;
  // bool _hasValidMime = false;
  // FileType _pickingType;
  // TextEditingController _controller = new TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String _fileName;
  List<PlatformFile> _paths;
  String _directoryPath;
  String _extension;
  bool _loadingPath = false;
  bool _multiPick = false;
  FileType _pickingType = FileType.any;
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.addListener(() => _extension = _controller.text);
  }

  void _openFileExplorer() async {
    setState(() => _loadingPath = true);
    try {
      _directoryPath = null;
      _paths = (await FilePicker.platform.pickFiles(
        type: _pickingType,
        allowMultiple: _multiPick,
        allowedExtensions: (_extension?.isNotEmpty ?? false)
            ? _extension?.replaceAll(' ', '').split(',')
            : null,
      ))
          ?.files;
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    } catch (ex) {
      print(ex);
    }
    if (!mounted) return;
    setState(() {
      _loadingPath = false;
      print(_paths.first.extension);
      _fileName =
          _paths != null ? _paths.map((e) => e.name).toString() : '...';
    });
  }

  void _clearCachedFiles() {
    FilePicker.platform.clearTemporaryFiles().then((result) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: result ? Colors.green : Colors.red,
          content: Text((result
              ? 'Temporary files removed with success.'
              : 'Failed to clean temporary files')),
        ),
      );
    });
  }

  void _selectFolder() {
    FilePicker.platform.getDirectoryPath().then((value) {
      setState(() => _directoryPath = value);
    });
  }

  String _valGender;
  List _listGender = ["Male", "Female"];

  

  @override

  // void initState() {
  //   super.initState();
  //   _controller.addListener(() => _extension = _controller.text);
  // }

  // void _openFile() async {
  //   if (_pickingType != FileType.custom || _hasValidMime){
  //     setState(() => _loadingPath = true);
  //     try {
  //       if (_multiPick) {
  //         _path = null;
  //         _paths = await FilePicker.getMultiFilePath(
  //           type: _pickingType, fileExtension: _extension);
  //       }else{
  //         _paths = null;
  //         _path = await FilePicker.getFilePath(
  //           type: _pickingType, fileExtension: _extension);
  //       }
  //     } on PlatformException catch (e) {
  //       print("Operation" + e.toString());
  //     }
  //     if (!mounted) return;
  //     setState(() {
  //       _loadingPath = false;
  //       _fileName = _path !=null
  //         ? _path.split('/').last
  //         : _paths != null ? _paths.keys.toString() : '...';
  //     });
  //   }
  // }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backwardsCompatibility: false,
        centerTitle: true,
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(8),
            bottomRight: Radius.circular(8),
          ),
        ),
        toolbarHeight: 70,
        title: Text(
          'PAID LEAVE',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 16,
          ),
        ),
        backgroundColor: bgColor,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(24, 35, 24, 90),
          child: Column(
            children: [
              Container(
                height: 35,
                width: 150,
                padding: EdgeInsets.only(
                  left: 40,
                  // right: 30,
                  top: 5,
                  bottom: 5,
                ),
                decoration: BoxDecoration(
                  color: bgColor,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: DropdownButton(
                  hint: Text(
                    'Kategori',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  icon: Icon(
                    Icons.arrow_drop_down,
                  ),
                  underline: SizedBox(),
                  iconEnabledColor: Colors.white,
                  value: _valGender,
                  items: _listGender.map((value) {
                    return DropdownMenuItem(
                      child: Text(value),
                      value: value,
                    );
                  }).toList(),
                  onChanged: (value) {},
                ),
              ),
              SizedBox(
                height: 36,
              ),
              TextFormField(
                cursorColor: bgColor,
                keyboardType: TextInputType.datetime,
                // autofocus: true,
                // style textfield
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  // buat background warna texfield
                  fillColor: Colors.white, filled: true,
                  hintText: 'Tanggal Cuti',
                  hintStyle: TextStyle(
                    color: Color(0xffA9A9A9),
                  ),
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 15.0,
                    horizontal: 20.0,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffA9A9A9),
                      width: 1.5,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffA9A9A9),
                      width: 1.5,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              SizedBox(
                width: double.infinity,
                child: TextFormField(
                  maxLines: 100 ~/ 20, // <--- maxLines
                  cursorColor: bgColor,
                  keyboardType: TextInputType.streetAddress,
                  // autofocus: true,
                  // style textfield
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                  ),

                  decoration: InputDecoration(
                    // buat background warna texfield
                    fillColor: Colors.white, filled: true,
                    hintText: 'Alasan Cuti',
                    hintStyle: TextStyle(
                      color: Color(0xffA9A9A9),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 17.0,
                      horizontal: 20.0,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xffA9A9A9),
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xffA9A9A9),
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              SizedBox(
                height: 192,
                width: double.infinity,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xffA9A9A9),
                      width: 1.5,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 50, bottom: 50),
                    child: Column(
                      children:[
                        ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.fromLTRB(10, 9.5, 10, 9.5),
                            primary: Colors.grey[100],
                            shape: RoundedRectangleBorder(
                              //to set border radius to button
                              borderRadius: BorderRadius.circular(4),
                            ),
                            side: BorderSide(
                              width: 1,
                              color: Color(0xffA3A3A3),
                            ), //bord
                          ),
                          label: Text(
                            'ATTACH A FILE',
                            style: TextStyle(
                              color: Color(0xff444444),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          icon: Icon(
                            Icons.add,
                            color: Color(0xff444444),
                          ),
                          onPressed: () => _openFileExplorer(),   
                        ),
                        Expanded(
                          child: Builder(
                            builder: (BuildContext context) => _loadingPath
                                ? Padding(
                                    padding: const EdgeInsets.only(left: 40, right: 40),
                                    child: const CircularProgressIndicator(),
                                  )
                                : _directoryPath != null
                                    ? ListTile(
                                        title: const Text('Directory path'),
                                        subtitle: Text(_directoryPath),
                                      )
                                    : _paths != null
                                        ? Container(
                                            padding: const EdgeInsets.only(top: 5, left: 40.0, right: 40.0),
                                            height:
                                                MediaQuery.of(context).size.height * 0.50,
                                            child: Scrollbar(
                                                child: ListView.separated(
                                              itemCount:
                                                  _paths != null && _paths.isNotEmpty
                                                      ? _paths.length
                                                      : 1,
                                              itemBuilder:
                                                  (BuildContext context, int index) {
                                                final bool isMultiPath =
                                                    _paths != null && _paths.isNotEmpty;
                                                final String name = 'File $index: ' +
                                                    (isMultiPath
                                                        ? _paths
                                                            .map((e) => e.name)
                                                            .toList()[index]
                                                        : _fileName ?? '...');
                                                final path = _paths
                                                    .map((e) => e.path)
                                                    .toList()[index]
                                                    .toString();

                                                return ListTile(
                                                  title: Text(
                                                    name,
                                                  ),
                                                  subtitle: Text(path),
                                                );
                                              },
                                              separatorBuilder:
                                                  (BuildContext context, int index) =>
                                                      const Divider(),
                                            )),
                                          )
                                        : const SizedBox(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    height: 40,
                    width: 82,
                    child: ElevatedButton(
                      child: Text('CANCEL'),
                      onPressed: () {
                        Navigator.pop(context);
                        print('cancel');
                      },
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          //to set border radius to button
                          borderRadius: BorderRadius.circular(4),
                        ),
                        primary: Color(0xff46D2A8),
                        // padding:
                        //     EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 13,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  SizedBox(
                    height: 40,
                    width: 69,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MainHomePage(),
                          ),
                        );
                        print('done');
                      },
                      child: Text('DONE'),
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          //to set border radius to button
                          borderRadius: BorderRadius.circular(4),
                        ),
                        primary: Color(0xff49A388),
                        // padding:
                        //     EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 13,
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
