

import 'package:bang_lukman/pages/personal_data/profile_page.dart';
import 'package:bang_lukman/shared/theme.dart';
import 'package:flutter/material.dart';

class PersonalDataPage extends StatefulWidget {

  @override
  _PersonalDataPageState createState() => _PersonalDataPageState();
}

class _PersonalDataPageState extends State<PersonalDataPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backwardsCompatibility: false,
        centerTitle: true,
        elevation: 0.1,
        shadowColor: bgColor,
        toolbarHeight: 70,
        title: Text(
          'ATTENDANCE',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 16,
          ),
        ),
        backgroundColor: bgColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Stack(
              children: [
                Container(
                  height: 150,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 4,
                        blurRadius: 10,
                        offset: Offset(0, 1)
                      ),
                    ],
                    color: bgColor,
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(8),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Stack(
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 20, left: 30, right: 30),
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: 42,
                              backgroundColor: Colors.white,
                              child: CircleAvatar(
                                radius: 41,
                                backgroundColor: bgColor,
                                child: CircleAvatar(
                                  radius: 36,
                                  backgroundColor: Colors.white,
                                  child: Text(
                                    'Foto',
                                    style: TextStyle(
                                      color: logoColor,
                                      fontSize: 25,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  RichText(
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.start,
                                    text: TextSpan(
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                      children: <TextSpan>[                                  
                                        TextSpan(
                                          text: 'Lukman Firdaus',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Text(
                                    'CEO',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                    textAlign: TextAlign.start,
                                  ),
                                  Text(
                                    'PT Robust Sejahtera',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                    textAlign: TextAlign.start,
                                  ),
                                ],
                              ), 
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'My Info',
                        style: TextStyle(
                          fontSize: 16, 
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 29,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.account_circle,
                            color:  bgColor,
                            size: 36.0,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Personal Info',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.expand_more_rounded ,
                              color: bgColor,
                              size: 30.0,
                            ),
                            onPressed: () {
                              Navigator.push(
                                context, 
                                MaterialPageRoute(
                                  builder:  (context) => ProfilePage(),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 29,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.account_box,
                            color:  bgColor,
                            size: 36.0,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Employment Info',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.expand_more_rounded ,
                              color: bgColor,
                              size: 30.0,
                            ),
                            onPressed: () {
                              
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 29,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.call,
                            color:  bgColor,
                            size: 36.0,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Emergency Contact Info',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.expand_more_rounded ,
                              color: bgColor,
                              size: 30.0,
                            ),
                            onPressed: () {
                             
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 29,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.groups_rounded,
                            color:  bgColor,
                            size: 36.0,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Family Info',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                            Icons.expand_more_rounded ,
                              color: bgColor,
                              size: 30.0,
                            ),
                            onPressed: () {
                              
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 29,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.menu_book_rounded,
                            color:  bgColor,
                            size: 36.0,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Education Info',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.expand_more_rounded ,
                              color: bgColor,
                              size: 30.0,
                            ),
                            onPressed: () {
                              
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 29,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.monetization_on_rounded,
                            color:  bgColor,
                            size: 36.0,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Payroll Info',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.expand_more_rounded ,
                              color: bgColor,
                              size: 30.0,
                            ),
                            onPressed: () {
                              
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 29,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.folder_open,
                            color:  bgColor,
                            size: 36.0,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'My File',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.expand_more_rounded ,
                              color: bgColor,
                              size: 30.0,
                            ),
                            onPressed: () {
                              
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Setting',
                        style: TextStyle(
                          fontSize: 16, 
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 29,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.settings_rounded,
                            color:  bgColor,
                            size: 36.0,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Change Password',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.expand_more_rounded ,
                              color: bgColor,
                              size: 30.0,
                            ),
                            onPressed: () {
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}