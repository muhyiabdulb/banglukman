import 'package:bang_lukman/shared/theme.dart';
import 'package:flutter/material.dart';

class ApprovalPage extends StatefulWidget {
  @override
  _ApprovalPageState createState() => _ApprovalPageState();
}

class _ApprovalPageState extends State<ApprovalPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backwardsCompatibility: false,
        centerTitle: true,
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(8),
            bottomLeft: Radius.circular(8),
          ),
        ),
        toolbarHeight: 70,
        title: Text(
          'APPROVAL',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 16,
          ),
        ),
        backgroundColor: bgColor,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(24, 100, 24, 90),
          child: Column(
            children: [
              TextFormField(
                cursorColor: bgColor,
                keyboardType: TextInputType.datetime,
                // autofocus: true,
                // style textfield
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  // buat background warna texfield
                  fillColor: Colors.white, filled: true,
                  hintText: 'Tanggal Izin',
                  hintStyle: TextStyle(
                    color: Color(0xffA9A9A9),
                  ),
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 15.0,
                    horizontal: 20.0,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffA9A9A9),
                      width: 1.5,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffA9A9A9),
                      width: 1.5,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              SizedBox(
                width: double.infinity,
                child: TextFormField(
                  maxLines: 100 ~/ 20, // <--- maxLines
                  cursorColor: bgColor,
                  keyboardType: TextInputType.streetAddress,
                  // autofocus: true,
                  // style textfield
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                  ),

                  decoration: InputDecoration(
                    // buat background warna texfield
                    fillColor: Colors.white, filled: true,
                    hintText: 'Alasan Izin',
                    hintStyle: TextStyle(
                      color: Color(0xffA9A9A9),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 17.0,
                      horizontal: 20.0,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xffA9A9A9),
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xffA9A9A9),
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              SizedBox(
                height: 192,
                width: double.infinity,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xffA9A9A9),
                      width: 1.5,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Center(
                    child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.fromLTRB(10, 9.5, 10, 9.5),
                        primary: Colors.grey[100],
                        shape: RoundedRectangleBorder(
                          //to set border radius to button
                          borderRadius: BorderRadius.circular(4),
                        ),
                        side: BorderSide(
                          width: 1,
                          color: Color(0xffA3A3A3),
                        ), //bord
                      ),
                      label: Text(
                        'ATTACH A FILE',
                        style: TextStyle(
                          color: Color(0xff444444),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      icon: Icon(
                        Icons.add,
                        color: Color(0xff444444),
                      ),
                      onPressed: () {
                        print('file');
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    height: 40,
                    width: 82,
                    child: ElevatedButton(
                      child: Text('CANCEL'),
                      onPressed: () {
                        print('cancel');
                      },
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          //to set border radius to button
                          borderRadius: BorderRadius.circular(4),
                        ),
                        primary: Color(0xff46D2A8),
                        // padding:
                        //     EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 13,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  SizedBox(
                    height: 40,
                    width: 69,
                    child: ElevatedButton(
                      onPressed: () {
                        print('done');
                      },
                      child: Text('DONE'),
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          //to set border radius to button
                          borderRadius: BorderRadius.circular(4),
                        ),
                        primary: Color(0xff49A388),
                        // padding:
                        //     EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 13,
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
