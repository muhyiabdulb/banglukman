import 'package:bang_lukman/pages/approval/approval_page.dart';
import 'package:bang_lukman/pages/attendance/attendance_page.dart';
import 'package:bang_lukman/pages/home/main_home_page.dart';
import 'package:bang_lukman/pages/personal_data/profile_page.dart';
import 'package:bang_lukman/shared/theme.dart';
import 'package:flutter/material.dart';

class BottomNavbarPage extends StatefulWidget {
  @override
  _BottomNavbarPageState createState() => _BottomNavbarPageState();
}

class _BottomNavbarPageState extends State<BottomNavbarPage> {
  int _selectedIndex = 0;

  final List<Widget> _contentPages = <Widget>[
    MainHomePage(),
    AttendancePage(),
    ApprovalPage(),
    ProfilePage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _contentPages.elementAt(_selectedIndex),
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(8),
          topLeft: Radius.circular(8),
        ),
        child: BottomNavigationBar(
          iconSize: 30,
          elevation: 1,
          backgroundColor: bgColor,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: Colors.white,
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.attachment,
                color: Colors.white,
              ),
              label: 'Attendance',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
                color: Colors.white,
              ),
              label: 'Profile',
            ),
          ],
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
