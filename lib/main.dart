import 'package:bang_lukman/pages/landing_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bang Lukman',
      theme: ThemeData(
        fontFamily: 'Poppins',
        primaryColor: Colors.white,
      ),
      home: LandingPage(),
    );
  }
}
